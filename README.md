# Parking App #

Application for finding parking spaces 

### What is this repository for? ###

* Contains code to handle viewing parking information on a website

### How do I get set up? ###

* Clone git repository
* Run pip install -r on requirements.txt
* Make all local django commands using python manage.py <command> --settings=parking_app.development_settings
* To run the application locally use python parking_app/manage.py runserver --settings=paring_app.development_settings