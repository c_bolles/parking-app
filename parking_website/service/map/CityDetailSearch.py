'''
TODO:
Add user validation.
Throw Exception when invalid city passed in
Handle multiple cities with same name
'''
from apps.parking_data_monitor.models import City

class CitySearch:
    
    def __init__(self, cityName):
        if(self.validSearch(cityName)):
            self.cityName = cityName
        else:
            pass
    
    def getJsonCityData(self):
        return(City.objects.all().filter(name = self.cityName).first().as_dict())
        
    def validSearch(self, cityName):
        return True