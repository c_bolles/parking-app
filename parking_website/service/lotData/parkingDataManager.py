'''
Handles data interaction between the Raspberry Pi and the client throught the web app.
This includes handling Raspberry Pi Post request by updating the database as well as loading
the default data at each start of the application as well as checking the status of the Raspberry Pis
'''
import urllib2
from apps.parking_data_monitor.models import Parking_Information, City
from Properties.MapProperties import defaultCityData

class ParkingDataManager():
    '''
    Updates the database with POST request from Raspberry pi. Takes in POST request and parses the information into the database.
    If the Raspberry pi made a request before, the information is updated. Otherwise, if the Raspberry pi is making a request for
    the first time, a new parkingInfoObject is inserted into the database.
    @param updateRequest POST request from Raspberry pi
    '''
    def updateInfo(self, updateRequest):
        #Split parameters ex)'url=google.com&lotId=2' becomes ['url=google.com', 'lotId=2']
        rawResponseList = updateRequest.body.split("&")
        
        responseDict = {}
        #Split parameters list into dictionary ex)['url=google.com', 'lotId=2'] becomes {'url' : 'google.com', 'lotId' : 2}
        for pair in rawResponseList:
            keyPair = pair.split('=')
            responseDict[keyPair[0]] = keyPair[1]
        #Store url in raspberry pi model
        currentInfo = Parking_Information.objects.filter(name=responseDict['name'])
        if len(currentInfo) > 0:
            currentInfo[0].url= "http://" + responseDict['piUrl']
            currentInfo[0].availableSpaces = responseDict['availableSpaces']
            currentInfo[0].isMonitored = True
            currentInfo[0].save()
        else:
            newInfo = Parking_Information(maxSpaces=responseDict['maxSpaces'], parkingType=responseDict['parkingType'], availableSpaces=responseDict['availableSpaces'], name=responseDict['name'], url=responseDict['piUrl'], address=responseDict['address'], lat=responseDict['latitude'], lng=responseDict['longitude'])
            newInfo.save()

    '''
    Loads the default data for the parking app into the database. The default data is loaded from the file "config.json" at the root of the app.
    The default data should only be called at the first call to the app. The default data is loaded only for newly added parking lots, exising
    data used exisiting information from database.
    '''
    def loadDefaultData(self):
        defaultCities = defaultCityData
        for city in defaultCities:
            existingCity = City.objects.filter(cityId = city['id'])
            if len(existingCity) == 0: #City with that id does not exist yet
                newCity = City(cityId = city['id'], name = city['name'], state = ['state'], lat = city['lat'], lng = city['lng'])
                newCity.save()
                existingCity = City.objects.filter(cityId = city['id']).first()
            for lot in city['parkingLots']:
                existingLots = Parking_Information.objects.filter(name = lot['name'])
                if len(existingLots) == 0:  #Parkinglot with that id does not exist yet
                    newParkingInfo = Parking_Information(availableSpaces = lot['availableSpaces'], maxSpaces = lot['maxSpaces'], parkingType=lot['parkingType'], name=lot['name'], url=lot['url'], address = lot['address'].replace('+', ' '), lat = lot['lat'], lng = lot['lng'])
                    newParkingInfo.save()
                    existingCity.first().parkingLots.add(newParkingInfo)
                else:
                    existingCity.first().parkingLots.add(existingLots[0])
            existingCity[0].save()

    '''
    Checks to see if a Raspberry pi is accessable through its given url
    @param url Url Raspberry Pi is suppose to be accessable at
    @return True if the url returns a 502 error. False if other HTTP error is thrown or Value Error thrown
    '''
    def urlAccessable(self, url):
        #Check to make sure url is still active
        try:
            response = urllib2.urlopen(url)
            if 'not found' in response.read():
                return False
        except urllib2.URLError, e:
            if str(502) in str(e):
                return True
            return False
        except ValueError, e:
            return False
        return False

    '''
    Updates the list of parking lots that are activly being monitored by Raspberry pi. Active Raspberry Pis will have an accessable url associated with them. If the Raspberry Pi is not
    accessable. The available parking statisics are disregarded.
    '''
    def updateMonitoredLots(self):
        parkingLots = Parking_Information.objects.all()
        #Loop through all Pi objects
        for lot in parkingLots:
            #If the url was never set, do not attempt to access url
            if self.urlAccessable(lot.url):
                lot.isMonitored = True
                lot.save()
            else:
                lot.isMonitored = False
                lot.save()
