'''
Controls where each specific url gets controlled from. This urls.py handles all requests from
www.ParkingInSaratoga.com/parking_map and divides responses up further
ex) www.ParkingInSaratoga.com/parking_map/postData is controled by the postData function in
view.py in the same folder.
'''

from django.conf.urls import url

from . import views
urlpatterns = [
    url(r'^citySearchText', views.getLocationDetails, name='getLocationDetails'),
    url(r'^getBestLot', views.getBestLot, name='getBestLot'),
    url(r'^getLotData', views.getLotData, name='getData'),
    url(r'^getLocationDetails', views.getLocationDetails, name='getLocationDetails'),
    url(r'^$', views.parkingMap, name='parkingMap'),
]