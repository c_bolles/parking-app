from __future__ import unicode_literals

from django.apps import AppConfig


class ParkingDataMonitorConfig(AppConfig):
    name = 'parking_data_monitor'
