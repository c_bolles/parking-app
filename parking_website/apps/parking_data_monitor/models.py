'''
Contains information used by Django to construct, maintain, and access Database stored information.
Each class represents a Table in the database
'''

from __future__ import unicode_literals

from django.db import models
'''
Represents all information for a given Parking Lot. Included in the information is the specific
coordinates of the lot for use by the Google Map API as well as user information such as
address and name. Also included is information on parking space availability of the lot

@param lotId (int) Unique integer value used to represent the parking lot in a computer friendly format
@param maxSpaces (int) The total number of spaces in the parking lot
@param parkingType (String) The type of parking lot ex) Garage, Open Air Lot, Street Parking 
@param availableSpaces (int) Parking Spaces left empty in the parking lot
@param name (String) Natural name for the parking lot generally taken from Saratoga Springs City ex) Walton
@param url (String) Ngrok generated url for the Raspberry Pi to be accessed at, used to check status of the Raspberry Pi.
@param address (String) Natural address of the parking lot ex) 1 Walton Avenue
@param lat (Float) Latitude of the parking lot for use by Google Maps API
@param lng (Float) Longitude of the parking lot for use by Google Maps API
@param isMonitored (boolean) Represents if the Raspberry Pi is activly updating the status of the Parking Lot
'''
class Parking_Information(models.Model):
    #Support varaibles
    parkingTypeChoices = (("Garage", "Garage"), ("Parking_Lot", "Parking Lot"), ("Street_Parking", "Street Parking"))
    
    #Database varibles
    lotId = models.AutoField(primary_key=True)
    maxSpaces = models.IntegerField()
    parkingType = models.CharField(max_length=40, choices=parkingTypeChoices, default="Garage")
    availableSpaces = models.IntegerField()
    name = models.CharField(max_length=60, default="Bob")
    url = models.CharField(max_length=60, default = "<empty>")
    address = models.CharField(max_length=80, default="")
    lat = models.DecimalField(max_digits=16, decimal_places=6, default = 10)
    lng = models.DecimalField(max_digits = 16, decimal_places=6, default = 10)
    isMonitored = models.BooleanField(default=False)
    
    #Supporting methods
    def __str__(self):
        return self.parkingType + ", " + self.name
    def as_json(self):
        return dict(lotId=self.lotId, maxSpaces=self.maxSpaces, parkingType=self.parkingType, availableSpaces=self.availableSpaces, name=self.name, url=self.url, address=self.address, lat=float(self.lat), lng=float(self.lng), isMonitored=self.isMonitored)

'''
Represents a message from the Raspberry Pi on the status of the parking lot. Used to communicate
between the web app, the Raspberry Pi and the user administration.

@param messageType (String) Represents the type of message being sent by the Raspberry Pi ex) Error, Warning, Alert messages
@param messageBody (String) Contains the main information being sent by the Raspberry Pi
@param parking_Info (Model) Refrence to the Parking_Info object that the Raspberry Pi is monitoring
'''
class Message(models.Model):
    #support varaibles
    messageTypes = (("Error", "Error"), ("Warning", "Warning"), ("Alert", "Alert"))
    
    #Database varibles
    messageType = models.CharField(max_length=60, choices = messageTypes, default="Alert")
    messageBody = models.CharField(max_length=300)
    parking_Info = models.ForeignKey(Parking_Information, on_delete=models.CASCADE)
    
    #Supporting methods
    def __str__(self):
        return self.messageType + " : " + self.messageBody
    
class City(models.Model):
    cityId = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=256)
    state = models.CharField(max_length=256)
    lat = models.DecimalField(max_digits=20, decimal_places=16)
    lng = models.DecimalField(max_digits=20, decimal_places=16)
    parkingLots = models.ManyToManyField(Parking_Information)
    postCode = models.IntegerField(default=12866)
    country = models.CharField(max_length=256, default='USA')
    
    def as_dict(self):
        lotData = []
        for lot in self.parkingLots.all():
            lotData.append(lot.as_json())
        return dict(cityId=self.cityId, name=self.name, state=self.state, lat=float(self.lat), lng=float(self.lng), lots=lotData)