# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-10-05 14:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parking_data_monitor', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='lat',
            field=models.DecimalField(decimal_places=60, max_digits=60),
        ),
        migrations.AlterField(
            model_name='city',
            name='lng',
            field=models.DecimalField(decimal_places=60, max_digits=60),
        ),
    ]
