from django.views.decorators.csrf import csrf_exempt
from service.lotData.parkingDataManager import ParkingDataManager
from django.http import HttpResponse


# Create your views here.
@csrf_exempt
def postData(request):
    dataManager = ParkingDataManager()
    if(request.method == 'POST'):
        dataManager.updateInfo(request)
        return HttpResponse(status=200)
    else:
        return HttpResponse(status=400)