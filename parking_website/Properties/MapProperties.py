defaultCityData = [
    {
        'id' : 1,
        'name' : 'Saratoga Springs',
        'state' : 'New York',
        'lat' : 43.0829962,
        'lng' : -73.7851426,
        'parkingLots' : [
            {
                "name" : "Woodlawn",
                "address" : "6 Woodlawn Ave",
                "availableSpaces" : -1,
                "maxSpaces" : 443,
                "parkingType" : "Garage",
                "url" : "<empty>",
                "lat" : 43.082582,
                "lng" : -73.7870628,
                "isMonitored" : False
            },
            {
                "name" : "Putnam",
                "address" : "21 Putnam Street",
                "availableSpaces" : -1,
                "maxSpaces" : 178,
                "parkingType" : "Garage",
                "url" : "<empty>",
                "lat" : 43.079967,
                "lng" : -73.784676,
                "isMonitored" : False
            },
            {
                "name" : "Walton",
                "address" : "37 Walton Street",
                "availableSpaces" : -1,
                "maxSpaces" : 219,
                "parkingType" : "Garage",
                "url" : "<empty>",
                "lat" : 43.0844903,
                "lng" : -73.7868252,
                "isMonitored" : False
            }
        ]
    }
]

googleAPIKey = 'AIzaSyBsyPIp3dHBpZf0b4lhBgi9MxnjZ6tiUpA'
googleMaxSearchRadius = 19000
googleMapDistanceAPIBaseUrl = 'https://maps.googleapis.com/maps/api/distancematrix/json?'
googleMapPlaceAPIBaseUrl = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'
googleMapType = "roadmap"
googleMapZoom = 15