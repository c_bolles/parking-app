/**
*Generates an array of Google Map markers to be used to populate the Google Map with
*the parking data as markers
*
*@param parkingData Contains all information of the parking lot as defined in the database. Marker data is extracted from here
*@returns Array of Google Map Markers
*/
function populateGoogleMapMarkers(parkingData)
{
    var googleMarkers = [];
    
    var greenMarker = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
    var orangeMarker = 'http://maps.google.com/mapfiles/ms/icons/orange-dot.png';
    var redMarker = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
    var purpleMarker = 'http://maps.google.com/mapfiles/ms/icons/purple-dot.png'
    for(var i = 0; i < parkingData.length; i++)
    {
        var percentAvailable = parkingData[i].availableSpaces/parkingData[i].maxSpaces;
        var color = "";
        
        //If less then 25% of spots are available
        if(!parkingData[i].isMonitored)
        {
            color = purpleMarker;
        }
        else if(percentAvailable < 0.25)
        {
            color = redMarker; 
        }
        else if(percentAvailable < 0.5)
        {
            color = orangeMarker;
        }
        else
        {
            color = greenMarker;
        }
        
        var marker = new google.maps.Marker({
            position : {lat : parkingData[i].lat, lng : parkingData[i].lng},
            title : parkingData[i].name,
        });
        marker.setIcon(color);
        googleMarkers.push(marker);
    }
    return googleMarkers;
}

function getTableElement(header, content)
{
    var contentString = '<tr>'+
                            '<th>' + header + '</th>'+
                            '<td>' + content + '</td>'+
                        '</tr>';
    return contentString;
}

/**
 *Generates the popup details that is shown when a user clicks on a Google Map marker.
 *Contains additional information on the parking lot including the number of available spaces
 *
 *@param parkingInfo Contains all information of the parking lot as defined in the database. Marker data is extracted from here
 *@returns String made up of HTML to be inserted into the websites HTML
 */
function getDetailView(parkingInfo)
{
        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h1 id="firstHeading" class="firstHeading"></h1>'+
            '<div id="bodyContent">'+
                '<table id="parking_information">';
        contentString += getTableElement("Name:", parkingInfo.name);
        contentString += getTableElement("Address:", parkingInfo.address);
        contentString += getTableElement("Parking Type:", parkingInfo.parkingType);
        if(parkingInfo.isMonitored)
        {
            contentString += getTableElement("Spots Available:", parkingInfo.availableSpaces);
        }
        contentString += '</table>'+
            '</div>'+
            '</div>';
    return contentString;
}

/**
 *Handles user clicks on the Google Map parkers by adding the additional HTML details view to
 *the HTML of the page
 *
 *@param parkingData Contains all information of the parking lot as defined in the database. Marker data is extracted from here
 *@returns An array of Sting objects each containg the specific information for each parking lot
*/
function populateInfoWindows(parkingData)
{
    var infoWindows = [];
    for(var i = 0; i < parkingData.length; i++)
    {
        var infoWindow = new google.maps.InfoWindow({
            content : getDetailView(parkingData[i])        
        });
        infoWindows.push(infoWindow);
    }
    return infoWindows;
}

var parkingData = null;
var googleMarkers = null;
var infoWindows = null;

/**
 *Loades the data for the first time by synchronously making GET request to web app
 */
function loadDataFirst()
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "/parking_map/getLotData", false);
    xhttp.send();
    if(xhttp.status == 200)
    {
        parkingData = JSON.parse(xhttp.responseText);
        googleMarkers = populateGoogleMapMarkers(parkingData);
        infoWindows = populateInfoWindows(parkingData);
    }
    return null;
}

/**
 *Continues to update data client side by syncronously updating data through GET request made at a certain time
 *interval as specified in the setInverval method call.
 */
window.setInterval(function loadDataSection()
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "/parking_map/getLotData", false);
    xhttp.send();
    if(xhttp.status == 200)
    {
        parkingData = JSON.parse(xhttp.responseText);
        googleMarkers = populateGoogleMapMarkers(parkingData);
        infoWindows = populateInfoWindows(parkingData);
    }
    return null;
}, 50000);

/**
 * Handles actual creation of the google map using the information passed into the function
 * @param centerPos {float[]} Array containing the latitude,longitude of the center of the town
 * @param zoom {int} Int representing how far to zoom into the map (usually 15 is good)
 * @param mapType {String} Type of google map to display (usually "Roadmap" is used)
 */
function createMap(latVal, lngVal, zoom, mapType)
{
    var map = new google.maps.Map(document.getElementById('map'),
    {
        zoom : zoom,
        center: {lat : latVal, lng : lngVal},
        mapTypeId: mapType
    });

    for(var i = 0; i < parkingData.length; i++)
    {
        (function()
        {
            var googleMarker = googleMarkers[i];
            var pos = i;
            googleMarker.setMap(map);
            googleMarker.addListener('click', function()
            {
                infoWindows[pos].open(map, googleMarker);
            });
        }());
    }
}

/**
 * Begins process of initializing the map. First the functions gets the location information before calling
 * another function to create the actual map
*/
function initMap()
{
    loadDataFirst();
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            parsedData = JSON.parse(xhttp.responseText);
            createMap(parsedData.lat, parsedData.lng, 15, 'roadmap');
        }
    };
    xhttp.open("GET", "/parking_map/getLocationDetails", true);
    xhttp.send();
}

function acceptableStringValue(val)
{
    return val != "" || val != null;
}


/**
 *Submit the data collected from the user for use in calculating the best lot
 */
function submitBestLotPicker()
{
var distanceFromOriginWeight = document.getElementById("distanceFromOriginWeight").value;
var distanceFromDestinationWeight = document.getElementById("distanceFromDestinationWeight").value;
var parkingAvailabilityWeight = document.getElementById("parkingAvailabilityWeight").value;
var url = window.location.origin + "/parking_map/getBestLot?currentLocation=" + 43.083224 + "," +  -73.784811
    + "&origin=" + document.getElementById("origin").value
    + "&destination=" + document.getElementById("destination").value
    + "&proximityToOriginWeight=" + distanceFromOriginWeight
    + "&proximityToDestinationWeight=" + distanceFromDestinationWeight
    + "&parkingAvailabilityWeight=" + parkingAvailabilityWeight;
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    var parsedData = JSON.parse(xhttp.responseText);
    var markerToClick = null;
    for(var i = 0; i < parkingData.length; i++)
    {
    	if(parkingData.name == parsedData[0].name)
    	{
    		markerToClick = googleMarkers[i];
    	}
    }
    google.maps.event.trigger(markerToClick, 'click')
    console.log(xhttp.responseText);
  }
};

xhttp.open("GET", url, true);
xhttp.send();
}

/**
 *Clear the data in the bestLotPicker UI to default values
 */
function cancelBestLotPicker()
{
    
}